**Prerequisites**
> 1. Node (recommended v20 or LTS)
> 2. Npm (recommended v9.6.x)


**Install dependency**

> `npm install`

**Build code**

> `tsc`

**Run unit test**

> `npm test`

