import { merge } from '../build/app'

describe('merge function', () => {
    it('merge 3 arrays and sorted with duplicate', () => {
        let col1 = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 16, 17, 19]
        let col2 = [18, 15, 14, 12, 11, 10, 9, 7, 6, 5, 3, 2]
        let col3 = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 13, 14, 16, 17, 18]
        let result = merge(col1, col2, col3)
        expect(result).toEqual([
            1, 
            2, 2, 2, 
            3, 3, 3, 
            4, 4, 
            5, 5, 
            6, 6, 6, 
            7, 7, 7, 
            8, 8, 
            9, 9, 9, 
            10, 10, 10, 
            11, 11, 11, 
            12, 12, 13, 
            14, 14, 14, 
            15, 
            16, 16, 
            17, 17, 
            18, 18,
            19
        ])
    })

    it('merge 3 arrays and sorted without duplicate', () => {
        let col1 = [5, 6, 14, 19]
        let col2 = [18, 15, 12, 11, 9, 2]
        let col3 = [1, 3, 4, 7, 8, 10, 13, 16, 17]
        let result = merge(col1, col2, col3)
        expect(result).toEqual([
            1,  2, 3, 4,  5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,  16, 17, 18, 19
        ])
    })

    it('all element are equal', () => {
        let col1 = [1, 1, 1, 1]
        let col2 = [3, 3, 3]
        let col3 = [5, 5, 5, 5, 5]
        let result = merge(col1, col2, col3)
        expect(result).toEqual([1, 1, 1, 1, 3, 3, 3, 5, 5 ,5 , 5, 5])
    })

    it('1 array is empty with duplicate', () => {
        let col1 = [5, 6, 6, 14, 19]
        let col2 = [18, 15, 12, 11, 9, 5, 2]
        let col3: number[] = []
        let result = merge(col1, col2, col3)
        expect(result).toEqual([
            2, 5, 5, 6, 6, 9, 11, 12, 14, 15, 18, 19
        ])
    })

    it('1 array is empty with duplicate', () => {
        let col1 = [5, 6, 6, 14, 19]
        let col2 = [18, 15, 12, 11, 9, 5, 2]
        let col3: number[] = []
        let result = merge(col1, col2, col3)
        expect(result).toEqual([
            2, 5, 5, 6, 6, 9, 11, 12, 14, 15, 18, 19
        ])
    })

})
