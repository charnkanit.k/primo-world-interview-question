export function merge(col1: number[], col2: number[], col3: number[]): number[] {
    const combinedArray = [...col1, ...col2, ...col3];
    const frequencyMap: { [key: number]: number } = {};

    combinedArray.forEach((num) => {
        if (frequencyMap[num]) {
            frequencyMap[num]++;
        } else {
            frequencyMap[num] = 1;
        }
    });

    let minKey = Number.POSITIVE_INFINITY ;
    let maxKey = 0 ;
    for (const key in frequencyMap) {
        const numKey = Number(key);
        if (numKey < minKey) {
            minKey = numKey;
        }
        if (numKey > maxKey) {
            maxKey = numKey;
        }
    }
    const sortedArray: number[] = [];
    for (let key = minKey; key <= maxKey; key++) {
        if (frequencyMap[key]) {
            for (let i = 0; i < frequencyMap[key]; i++) {
                sortedArray.push(key);
            }
        }
    }

    return sortedArray;
}
